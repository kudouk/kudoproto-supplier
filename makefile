VERSION = "unset"
DATE=$(shell date -u +%Y-%m-%d-%H:%M:%S-%Z)
DOCKER_IMAGE ?=	kudo/admin-api-gateway
IMAGE_TAG ?= latest

codegen:
	cp ../../services/kudo-service/proto/**/*.d.proto ./
	cp ../../services/kudo-service/proto/**/*.supplier.api.proto ./
	cp ../../services/supplier-service/proto/**/*.d.proto ./
	cp ../../services/supplier-service/proto/**/*.m.proto ./
	cp ../../services/supplier-service/proto/**/*.supplier.api.proto ./
	cp ../../services/cart-service/proto/**/*.m.proto ./
	cp ../../services/specification-service/proto/**/*.m.proto ./
	cp ../../services/specification-service/proto/**/*.supplier.api.proto ./
	cp ../../services/order-service/proto/**/*.m.proto ./
	cp ../../services/order-service/proto/**/*.supplier.api.proto ./
	cp ../../services/media-service/proto/**/*.m.proto ./
	cp ../../services/media-service/proto/**/*.supplier.api.proto ./
	cp ../../services/notification-service/proto/supplier/*.m.proto ./
	cp ../../services/notification-service/proto/supplier/*.supplier.api.proto ./
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		-I${GOPATH}/pkg/mod \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		facet.d.proto image.d.proto \
		attribute.d.proto collection.d.proto product.d.proto product.supplier.api.proto \
		supplierAdmin.d.proto supplierAdmin.m.proto supplierAdmin.supplier.api.proto \
		supplier.d.proto supplier.m.proto supplier.supplier.api.proto \
		supplierTemp.d.proto supplierTemp.m.proto supplierTemp.supplier.api.proto \
		specification.m.proto task.m.proto tender.m.proto specification.supplier.api.proto \
		cart.m.proto order.m.proto order.supplier.api.proto \
		media.m.proto media.supplier.api.proto \
		notification.m.proto notification.supplier.api.proto

supplier:
	cp ../../kudo-supplier/proto/**/*.d.proto ./
	cp ../../kudo-supplier/proto/**/*.m.proto ./
	cp ../../kudo-supplier/proto/**/*.supplier.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		supplierAdmin.d.proto supplierAdmin.m.proto supplierAdmin.supplier.api.proto \
		supplier.d.proto supplier.m.proto supplier.supplier.api.proto \
		supplierTemp.d.proto supplierTemp.m.proto supplierTemp.supplier.api.proto

kudo:
	cp ../../kudo-service/proto/**/*.d.proto ./
	cp ../../kudo-service/proto/**/*.supplier.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		facet.d.proto collection.d.proto attribute.d.proto image.d.proto product.d.proto product.supplier.api.proto

notification:
	cp ../../kudo-notification/proto/supplier/*.m.proto ./
	cp ../../kudo-notification/proto/supplier/*.supplier.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
	notification.m.proto notification.supplier.api.proto

specification:
	cp ../../kudo-specification/proto/specification/*.m.proto ./
	cp ../../kudo-specification/proto/specification/*.supplier.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
	specification.m.proto task.m.proto tender.m.proto specification.supplier.api.proto

order:
	cp ../../kudo-orders/proto/**/*.m.proto ./
	cp ../../kudo-cart/proto/**/*.m.proto ./
	cp ../../kudo-orders/proto/**/*.supplier.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		cart.m.proto order.m.proto order.supplier.api.proto

media:
	cp ../../kudo-media/proto/**/*.m.proto ./
	cp ../../kudo-media/proto/**/*.supplier.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		media.m.proto media.supplier.api.proto